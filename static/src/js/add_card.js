/**
 * Created by DEV on 11/06/2016.
 */
import {loadBoards} from './get_borads'


document.getElementById("Add_card").addEventListener("click", build_option_list);
document.getElementById("Cencel_card_button").addEventListener("click", cencel);
let Lists=[];

function import_lists() {
    var myModule = require('./get_borads');
    Lists = myModule.Lists;
}

function build_option_list() {
    $('#add_card_bar').show();
    if (document.getElementById("list_add").options.length==0){
        import_lists();
        for (let list of Lists) {
            var option = document.createElement("option");
            option.setAttribute('id', list.id);
            option.appendChild(document.createTextNode(list.name));
            document.getElementById('list_add').appendChild(option);
        }
    }

}



function AddTesk () {
  var index = document.getElementById("list_add").selectedIndex;
  var mySelect= document.getElementById("list_add").getElementsByTagName("option")[index];
  var text = document.getElementById('card_text').value;

  var newCard =
  {
      key:'b344155ad05c50ff257a78a4a681ae96',
      name:text,
      pos: "top",
      due: null,
      idList: mySelect.id
  };

send_card(newCard);

};

function send_card(newCard){
      Trello.post(
      '/cards/',
      newCard,
      function() {
          $('#add_card_bar').hide();
          document.getElementById('card_text').value='';
          add_new_card_to_list(newCard);
      }
    );
}

function add_new_card_to_list(newCard){
     $('#list_add').html("");
    $(`#${newCard.idList}`).append(`<p id="card_note">${newCard.name}<br></p>`);

}

function loadCards(cards) {
      $(`#${cards[0].idList}`).append("<p id='card_note'>" + cards[0].name +"<br></p>");
     $('#list_add').html("");
}

function cencel(){
    $('#list_add').html("");
    $('#add_card_bar').hide();
}
$(document).ready(function () {
     $("#add_card_button").click(function (e) {
         AddTesk();

     });
});